﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace School_Register
{
    class Votes
    {
        private SQLiteConnection sqlite;
        Students tempStudent = new Students();
        public Votes()
        {
            sqlite = new SQLiteConnection("Data Source=./school_db.db");
        }
        public DataTable getVotes(string voteID)
        {
            //TODO: seconda query per riempire la gridView
            DataTable votesTable = new DataTable();

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection())
                {
                    SQLiteCommand cmd;
                    //sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    string query = "SELECT Votes.Subject, Votes.Vote  From Students, Votes WHERE Students.Name = @ID and Votes.IDStudent = Students.ID;";
                    cmd.CommandText = query;  //set the passed query
                    cmd.Parameters.Add(new SQLiteParameter("@ID", voteID));
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(votesTable); //fill the datasource
                    conn.Close();
                }
            }
            catch (SQLiteException)
            {
                MessageBox.Show("Errro db student");
            }
            return votesTable;
        }
        

        public DataTable addVotes(int thisVote , string thisNameValue, string idsubject)  //thisNameValue = name of student
        {
            string tempId = tempStudent.getStudentID(thisNameValue);
            string idDelloStudente = tempId.ToString();

            //TODO: seconda query per riempire la gridView
            DataTable votesTable = new DataTable();
  
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection())
                {
                    SQLiteCommand cmd;
                    //sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    string query = "insert into Votes(IDStudent, Subject, Vote) values(@IDStudent, @IDSubject, @IDVote)";
                    cmd.CommandText = query;  //set the passed query
                
                    cmd.Parameters.Add(new SQLiteParameter("@IDStudent", idDelloStudente));
                    cmd.Parameters.Add(new SQLiteParameter("@IDSubject", idsubject));
                    cmd.Parameters.Add(new SQLiteParameter("@IDVote", thisVote));
                    Console.WriteLine("nome " + idDelloStudente);
                    Console.WriteLine(idsubject);
                    Console.WriteLine(thisVote);


                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(votesTable); //fill the datasource

                   

                    conn.Close();
                }
            }
            catch (SQLiteException)
            {
                MessageBox.Show("Errro db votes");
            }
            return votesTable;
        }


    }
}
