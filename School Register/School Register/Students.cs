﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace School_Register
{
    class Students
    {
        private SQLiteConnection sqlite;

        public Students()
        {
            sqlite = new SQLiteConnection("Data Source=./school_db.db");
        }

        public DataTable getStudent(string classID)
        {
            DataTable studentTable = new DataTable();

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection())
                {
                    SQLiteCommand cmd;
                    //sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    //string query = "select Name from Students where ClassID = @ID";
                    string query = "SELECT Students.Name, Students.ClassID, Students.BornDate From Students WHERE Students.ClassID = @ID;";
                    cmd.CommandText = query;  //set the passed query
                    cmd.Parameters.Add(new SQLiteParameter("@ID", classID));
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(studentTable); //fill the datasource
                    conn.Close();
                }
            }
            catch (SQLiteException)
            {
                MessageBox.Show("Errrore riscontrato durante il download della lista studenti");
            }
            return studentTable;
        }

        public string getStudentID(string nameValue)  //name of student 
        {
            string desc = "";
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection())
                {
                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();

                    string query = "Select ID from Students WHERE Name = @Name";

                    cmd.CommandText = query;  //set the passed query
                    cmd.Parameters.Add(new SQLiteParameter("@Name", nameValue));

                    SQLiteDataReader reader = cmd.ExecuteReader();
                    {
                        while (reader.Read())
                        {
                            desc = reader["ID"].ToString();
                            Console.WriteLine("ID: {0}\n ", desc);
                        }
                    }

                    conn.Close();
                }
            }
            catch (SQLiteException)
            {
                MessageBox.Show("Errrore riscontrato durante il download dell'ID dell' ultimo studente selezionato");
            }
            return desc;
        }

        public DataTable addStudent(string newName, string newClass, string newDate)
        {
            DataTable addStudentTable = new DataTable(); 
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection())
                {
                    
                    SQLiteCommand cmd;
                    //sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    //string query = "select Name from Students where ClassID = @ID";
                    string query = "insert into Students (Name, ClassID, BornDate) values (@Name, @ClassID, @BornDate)";
                    cmd.CommandText = query;  //set the passed query
                    cmd.Parameters.Add(new SQLiteParameter("@Name", newName));
                    cmd.Parameters.Add(new SQLiteParameter("@ClassID", newClass));
                    cmd.Parameters.Add(new SQLiteParameter("@BornDate", newDate));
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(addStudentTable); //fill the datasource

                    conn.Close();
                }
            }
            catch (SQLiteException)
            {
                MessageBox.Show("Errro db student");
            }
            return addStudentTable;
        }
    }
}
