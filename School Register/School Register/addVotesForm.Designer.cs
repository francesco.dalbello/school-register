﻿namespace School_Register
{
    partial class addVotesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subjectTextbox = new System.Windows.Forms.TextBox();
            this.subjectLabel = new MaterialSkin.Controls.MaterialLabel();
            this.votesCombox = new System.Windows.Forms.ComboBox();
            this.inputVoteLabel = new MaterialSkin.Controls.MaterialLabel();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // subjectTextbox
            // 
            this.subjectTextbox.Location = new System.Drawing.Point(22, 62);
            this.subjectTextbox.Name = "subjectTextbox";
            this.subjectTextbox.Size = new System.Drawing.Size(100, 20);
            this.subjectTextbox.TabIndex = 0;
            // 
            // subjectLabel
            // 
            this.subjectLabel.AutoSize = true;
            this.subjectLabel.Depth = 0;
            this.subjectLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.subjectLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.subjectLabel.Location = new System.Drawing.Point(18, 28);
            this.subjectLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.subjectLabel.Name = "subjectLabel";
            this.subjectLabel.Size = new System.Drawing.Size(60, 19);
            this.subjectLabel.TabIndex = 1;
            this.subjectLabel.Text = "Materia";
            // 
            // votesCombox
            // 
            this.votesCombox.FormattingEnabled = true;
            this.votesCombox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.votesCombox.Location = new System.Drawing.Point(22, 140);
            this.votesCombox.Name = "votesCombox";
            this.votesCombox.Size = new System.Drawing.Size(121, 21);
            this.votesCombox.TabIndex = 2;
            // 
            // inputVoteLabel
            // 
            this.inputVoteLabel.AutoSize = true;
            this.inputVoteLabel.Depth = 0;
            this.inputVoteLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.inputVoteLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.inputVoteLabel.Location = new System.Drawing.Point(18, 104);
            this.inputVoteLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.inputVoteLabel.Name = "inputVoteLabel";
            this.inputVoteLabel.Size = new System.Drawing.Size(120, 19);
            this.inputVoteLabel.TabIndex = 3;
            this.inputVoteLabel.Text = "Seleziona il voto";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(22, 189);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(100, 36);
            this.materialRaisedButton1.TabIndex = 4;
            this.materialRaisedButton1.Text = "Conferma";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // addVotesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 245);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.inputVoteLabel);
            this.Controls.Add(this.votesCombox);
            this.Controls.Add(this.subjectLabel);
            this.Controls.Add(this.subjectTextbox);
            this.Name = "addVotesForm";
            this.Text = "addVotesForm";
            this.Load += new System.EventHandler(this.addVotesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox subjectTextbox;
        private MaterialSkin.Controls.MaterialLabel subjectLabel;
        private System.Windows.Forms.ComboBox votesCombox;
        private MaterialSkin.Controls.MaterialLabel inputVoteLabel;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
    }
}