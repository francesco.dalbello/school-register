﻿using MaterialSkin.Controls;
using MaterialSkin.Animations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace School_Register
{

    public partial class Form1 : MaterialForm
    {

        Classes myClasses = new Classes();
        Students myStudents = new Students();
        Votes myVotes = new Votes();

        string nameValue = "";
        string classID = "";
        List<string> classesList = new List<string>();

        public Form1()
        {
            InitializeComponent();
            btnRunTimeClasses();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void btnRunTimeClasses()
        {
            var counterItem = myClasses.getClass();
            int y = 0;
            
            foreach (DataRow riga in counterItem.Rows)
            {
                MaterialRaisedButton button = new MaterialRaisedButton();
                DataGridViewButtonColumn gridBtn = new DataGridViewButtonColumn();
                button.Width = 100;
                button.Height = 50;
                button.Location = new Point(12, 20+y);
                button.Text = riga[0].ToString();
                classesList.Add(button.Text);

                button.Click += delegate                
                {
                    //actions of the button
                    classID = button.Text;
                   
                    fillDataGrid();
                    materialTabControl1.SelectedTab = Classe;
                    classesGrid.Refresh();
                };
                mieClassi.Controls.Add(button);
                button.Show();
                y += 80;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            tabSelectorButton.Width = this.Width;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void tabSelectorButton_Click(object sender, EventArgs e)
        {

        }

        private void studentsGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //value of cell that i clicked
           
            nameValue = classesGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
            Console.WriteLine("primo name value: " + nameValue);
            if(e.ColumnIndex == 0)
            {
                btnRunTimeVotes(nameValue);
            }
        }

        public void btnRunTimeVotes(string nameValue)
        {
            materialTabControl1.SelectedTab = Studenti;
            var votsList = myVotes.getVotes(nameValue);
            studentsGrid.DataSource = votsList;
            refreshGrid();
        }

        private void Studenti_Click(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            //bottone per aggiungere uno studente
            addStudentForm myInputForm = new addStudentForm(classesList);
            if(myInputForm.ShowDialog() == DialogResult.OK)
            {
                refreshGrid();
            }

        }

        public void refreshGrid()  
        {
            fillDataGrid();
            classesGrid.Refresh();
            studentsGrid.Refresh();

        }

        public void fillDataGrid()
        {
            var studentsList = myStudents.getStudent(classID);

            classesGrid.DataSource = studentsList;
       
            /////////////////////////////////////////

            var votesList = myVotes.getVotes(nameValue);
            studentsGrid.DataSource = votesList;
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            //bottome per aggiungere voti
            addVotesForm myVoteInputForm = new addVotesForm(nameValue);
            if (myVoteInputForm.ShowDialog() == DialogResult.OK)
            {
                refreshGrid();
            }
        }
    }
}
