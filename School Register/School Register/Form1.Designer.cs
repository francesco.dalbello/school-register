﻿namespace School_Register
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabSelectorButton = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.mieClassi = new System.Windows.Forms.TabPage();
            this.Classe = new System.Windows.Forms.TabPage();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.classesGrid = new System.Windows.Forms.DataGridView();
            this.Studenti = new System.Windows.Forms.TabPage();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.studentsGrid = new System.Windows.Forms.DataGridView();
            this.materialTabControl1.SuspendLayout();
            this.Classe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.classesGrid)).BeginInit();
            this.Studenti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabSelectorButton
            // 
            this.tabSelectorButton.BaseTabControl = this.materialTabControl1;
            this.tabSelectorButton.Depth = 0;
            this.tabSelectorButton.Location = new System.Drawing.Point(0, 64);
            this.tabSelectorButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabSelectorButton.Name = "tabSelectorButton";
            this.tabSelectorButton.Size = new System.Drawing.Size(713, 50);
            this.tabSelectorButton.TabIndex = 1;
            this.tabSelectorButton.Text = "materialTabSelector1";
            this.tabSelectorButton.Click += new System.EventHandler(this.tabSelectorButton_Click);
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.mieClassi);
            this.materialTabControl1.Controls.Add(this.Classe);
            this.materialTabControl1.Controls.Add(this.Studenti);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Location = new System.Drawing.Point(12, 120);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(683, 349);
            this.materialTabControl1.TabIndex = 2;
            // 
            // mieClassi
            // 
            this.mieClassi.Location = new System.Drawing.Point(4, 22);
            this.mieClassi.Name = "mieClassi";
            this.mieClassi.Padding = new System.Windows.Forms.Padding(3);
            this.mieClassi.Size = new System.Drawing.Size(675, 323);
            this.mieClassi.TabIndex = 0;
            this.mieClassi.Tag = "0";
            this.mieClassi.Text = "Le mie classi";
            this.mieClassi.UseVisualStyleBackColor = true;
            this.mieClassi.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // Classe
            // 
            this.Classe.Controls.Add(this.materialRaisedButton1);
            this.Classe.Controls.Add(this.classesGrid);
            this.Classe.Location = new System.Drawing.Point(4, 22);
            this.Classe.Name = "Classe";
            this.Classe.Size = new System.Drawing.Size(675, 323);
            this.Classe.TabIndex = 1;
            this.Classe.Tag = "1";
            this.Classe.Text = "Classe";
            this.Classe.UseVisualStyleBackColor = true;
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(592, 30);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(50, 35);
            this.materialRaisedButton1.TabIndex = 1;
            this.materialRaisedButton1.Text = "ADD";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // classesGrid
            // 
            this.classesGrid.AllowUserToAddRows = false;
            this.classesGrid.AllowUserToDeleteRows = false;
            this.classesGrid.AllowUserToResizeColumns = false;
            this.classesGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.classesGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.classesGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.classesGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.classesGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.classesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.classesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.classesGrid.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.classesGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.classesGrid.GridColor = System.Drawing.Color.Black;
            this.classesGrid.Location = new System.Drawing.Point(9, 14);
            this.classesGrid.Name = "classesGrid";
            this.classesGrid.ReadOnly = true;
            this.classesGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.classesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.classesGrid.RowHeadersVisible = false;
            this.classesGrid.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.classesGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.classesGrid.Size = new System.Drawing.Size(647, 295);
            this.classesGrid.TabIndex = 0;
            this.classesGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.studentsGrid_CellClick);
            // 
            // Studenti
            // 
            this.Studenti.Controls.Add(this.materialRaisedButton2);
            this.Studenti.Controls.Add(this.studentsGrid);
            this.Studenti.Location = new System.Drawing.Point(4, 22);
            this.Studenti.Name = "Studenti";
            this.Studenti.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Studenti.Size = new System.Drawing.Size(675, 323);
            this.Studenti.TabIndex = 2;
            this.Studenti.Tag = "2";
            this.Studenti.Text = "I miei studenti";
            this.Studenti.UseVisualStyleBackColor = true;
            this.Studenti.Click += new System.EventHandler(this.Studenti_Click);
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(592, 30);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(50, 35);
            this.materialRaisedButton2.TabIndex = 2;
            this.materialRaisedButton2.Text = "ADD";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // studentsGrid
            // 
            this.studentsGrid.AllowUserToAddRows = false;
            this.studentsGrid.AllowUserToDeleteRows = false;
            this.studentsGrid.AllowUserToResizeColumns = false;
            this.studentsGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            this.studentsGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.studentsGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.studentsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.studentsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.studentsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.studentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.studentsGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.studentsGrid.GridColor = System.Drawing.Color.Black;
            this.studentsGrid.Location = new System.Drawing.Point(9, 14);
            this.studentsGrid.Margin = new System.Windows.Forms.Padding(0);
            this.studentsGrid.Name = "studentsGrid";
            this.studentsGrid.ReadOnly = true;
            this.studentsGrid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.studentsGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.studentsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.studentsGrid.RowHeadersVisible = false;
            this.studentsGrid.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.studentsGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.studentsGrid.Size = new System.Drawing.Size(647, 295);
            this.studentsGrid.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(707, 492);
            this.Controls.Add(this.tabSelectorButton);
            this.Controls.Add(this.materialTabControl1);
            this.ForeColor = System.Drawing.Color.Aqua;
            this.Name = "Form1";
            this.Text = "School Register";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.materialTabControl1.ResumeLayout(false);
            this.Classe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.classesGrid)).EndInit();
            this.Studenti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.studentsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private MaterialSkin.Controls.MaterialTabSelector tabSelectorButton;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage mieClassi;
        private System.Windows.Forms.TabPage Classe;
        private System.Windows.Forms.TabPage Studenti;
        private System.Windows.Forms.DataGridView classesGrid;
        private System.Windows.Forms.DataGridView studentsGrid;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
    }
}

