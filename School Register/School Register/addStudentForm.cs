﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace School_Register
{
    public partial class addStudentForm : Form
    {
        Students addStudents = new Students();
        private List<string> classesList1;

        public addStudentForm(List<string> classesList1)
        {
            InitializeComponent();
            this.classesList1 = classesList1;

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void addStudentForm_Load(object sender, EventArgs e)
        {
            inputClassByForm.DataSource = classesList1;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            string newName = inputNameByForm.Text; //textbox
            string newClass = inputClassByForm.Text; //combox

            //TODO: fixare formato input datetime(yyyy-mm-dd)
            string newDate = inputBornDate.Value.ToString("yyyyMMdd");
            addStudents.addStudent(newName, newClass, newDate);

            this.DialogResult = DialogResult.OK;
            this.Close(); //close the form
        }
    }
}
