﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace School_Register
{
    class Classes
    {
        //TODO: devo cancellare la classe SQL e fare le connessioni e comandi da ogni singola classe,
        //copiando e incollando il codice

        private SQLiteConnection sqlite;
        public Classes()
        {
            sqlite = new SQLiteConnection("Data Source=./school_db.db");
        }

        public DataTable getClass()
        {
            DataTable counter = new DataTable();

            try
            {
                using (SQLiteConnection a = new SQLiteConnection())
                {
                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    string query = "SELECT DISTINCT ClassID FROM Students;";
                    cmd.CommandText = query;  //set the passed query
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(counter); //fill the datasource
                    a.Close();
                }
            }
            catch (SQLiteException e)
            {
                MessageBox.Show("Errore ricontrato durante il download delle classi dal db");
            }
            return counter;
        }
    }
}
