﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace School_Register
{
    public partial class addVotesForm : Form
    {
        int[] votesAray = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        public string nameValue;
        Students getMyStudentID = new Students();
        Votes addMyVotes = new Votes();


        public addVotesForm(string nameValue)
        {
            InitializeComponent();
            this.nameValue = nameValue;
            Console.WriteLine("namevalue: " + nameValue);
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            int thisVote = int.Parse(votesCombox.Text);
            string subjectid = subjectTextbox.Text;
            

            addMyVotes.addVotes(thisVote, nameValue, subjectid);

            this.DialogResult = DialogResult.OK;
            this.Close(); //close the form
        }

        private void addVotesForm_Load(object sender, EventArgs e)
        {

        }
    }
}
